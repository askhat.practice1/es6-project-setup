# GIT_SSH_COMMAND='ssh -i ~/.ssh/gitlab-ssh-askhat.practice1' git clone git@gitlab.com:askhat.practice1/9.1.es6.git

DOCKER_COMPOSE=docker-compose
APP=nodejs

.PHONY: all ps logs up run stop down test shell pull node bash

all: ps

ps: 
	$(DOCKER_COMPOSE) ps

logs:
	$(DOCKER_COMPOSE) logs -f --tail 100

up:
	$(DOCKER_COMPOSE) up -d --build
	$(DOCKER_COMPOSE) ps

run:
	$(DOCKER_COMPOSE) up --build

stop:
	$(DOCKER_COMPOSE) stop

down:
	$(DOCKER_COMPOSE) down

test:
	$(DOCKER_COMPOSE) run $(APP) sh -c "npm install && npm test"

shell: 
	$(DOCKER_COMPOSE) exec $(APP) bash
	
pull:
	git config core.sshCommand "ssh -i ~/.ssh/gitlab-ssh-askhat.practice1 -F /dev/null"
	git pull

push:
	git config core.sshCommand "ssh -i ~/.ssh/gitlab-ssh-askhat.practice1 -F /dev/null"
	git push

node:
	$(DOCKER_COMPOSE) run $(APP) sh -c "node" 

bash:
	$(DOCKER_COMPOSE) run $(APP) sh -c "bash"
